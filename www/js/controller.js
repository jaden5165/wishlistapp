angular.module('starter.controllers', [])

.controller('HomeCtrl', ['$scope', '$state', '$ionicHistory', '$window', 'Auth', 'SessionData', function($scope, $state, $ionicHistory, $window, Auth, SessionData){

	$scope.login = {};
	$scope.signin = function(){
		var username = $scope.login.username;
		var password = $scope.login.password;

		console.log(username, password);

		Auth.$authWithPassword({
        	email: username,
        	password: password
    	})
    	.then(function(user) {
        	//Success callback
        	console.log('Authentication successful');
        	SessionData.setUser(username);
        	$state.go('userHome');
        }, function(error) {
        	//Failure callback
        	console.log('Authentication failure');
        });
	};

	$scope.showSignUp = function(){
    	$state.go('signUp');
	};

}])

.controller('UserHomeCtrl', ['$scope', '$state', '$ionicHistory', 'Auth', 'WishFb', function($scope, $state, $ionicHistory, Auth, WishFb){

	$scope.wishes = WishFb;
	$scope.logout = function(){
		Auth.$unauth();
		$state.go('home');
	};

	$scope.showAddWish = function(){
    	$state.go('addWish');
	};
	
}])

.controller('SignUpCtrl', ['$scope', '$state', 'Auth', function($scope, $state, Auth){
	
	$scope.login = {};

	$scope.signup = function(){
		var username = $scope.login.username;
		var password = $scope.login.password;

		console.log(username, password);

		Auth.$createUser({
        	email: username,
        	password: password
    	})
    	.then(function(user) {
        	//Success callback
        	console.log('Sign Up successful');
        	$state.go('userHome');
        }, function(error) {
        	//Failure callback
        	console.log('Sign Up failure');
        });
	};

	$scope.showSignIn = function(){
		$state.go('home');
	};


}])

.controller('AddWishCtrl', ['$scope','$state', '$firebase', 'WishFb', 'SessionData', function($scope,$state, $firebase, WishFb, SessionData){

	$scope.user = {};
	$scope.wishes = WishFb;

    $scope.showUserHome = function(){
        $state.go('userHome');
    };

    $scope.add = function(){
        var user = SessionData.getUser();

        $scope.wishes.$add({
            wish: $scope.user.wish,
            email: user
        }).then(function(ref) {
            console.log(ref);
            $state.go('userHome');
        }, function(error) {
            console.log("Error:", error);
        });
    };
}]);